namespace Quantities.Test

open NUnit.Framework
open FsCheck.NUnit
open Quantities

module ConverterTest =

    let private converterRoundtrip converter value =
        (value |> Converter.convertTo converter |> Converter.convertFrom converter)

    [<Property>]
    let ``Identity roundtrip should be identity`` (input: decimal) =
        let value = Fractions.Fraction(input)
        (value |> converterRoundtrip Converter.identity) = value

    [<Property>]
    let ``Linear roundtrip should be identity`` (input: decimal) =
        let value = Fractions.Fraction(input)
        let converter = Converter.linear (Numeric.fromInt 10)
        (value |> converterRoundtrip converter) = value

    [<Property>]
    let ``Affine roundtrip should be identity`` (input: decimal) =
        let value = Fractions.Fraction(input)
        let converter = Converter.affine (Numeric.fromInt 10) (Numeric.fromInt 5)
        (value |> converterRoundtrip converter) = value

    [<Property>]
    let ``Composite roundtrip should be identity`` (input: decimal) =
        let value = Fractions.Fraction(input)

        let converter =
            Converter.compose
                (Converter.linear (Numeric.fromInt 10))
                (Converter.affine (Numeric.fromInt 3) (Numeric.fromInt 5))

        (value |> converterRoundtrip converter) = value

    [<Property>]
    let ``Inverse roundtrip should be identity`` (input: decimal) =
        let value = Fractions.Fraction(input)

        let converter =
            Converter.inverse (Converter.affine (Numeric.fromInt 3) (Numeric.fromInt 5))

        (value |> converterRoundtrip converter) = value
