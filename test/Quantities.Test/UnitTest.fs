namespace Quantities.Test

open NUnit.Framework
open Quantities


module UnitsDefTest =
    let private Joules = UnitDef.Simple("joules", "J")
    let private KiloJoules = UnitDef.Composite(UnitDef.Kilo, Joules)

    [<Test>]
    let ``Simple unit should have a symbol`` () =
        Assert.AreEqual("J", UnitDef.symbolOf Joules)

    [<Test>]
    let ``Prefix unit should have a symbol`` () =
        Assert.AreEqual("m", UnitDef.symbolOf UnitDef.Milli)

    [<Test>]
    let ``Composite unit should have a symbol`` () =
        Assert.AreEqual("kJ", UnitDef.symbolOf KiloJoules)

    [<Test>]
    let ``RootOf Simple unit should return the same unit`` () =
        Assert.AreEqual(Joules, UnitDef.rootOf Joules)

    [<Test>]
    let ``RootOf Composite unit should return the underlying unit`` () =
        Assert.AreEqual(Joules, UnitDef.rootOf KiloJoules)
