﻿namespace Model


open FSharp.Data.UnitSystems.SI.UnitSymbols

// @dataclass(frozen=True)
// class Source:
//     value: str


// Name = NewType("Name", str)
// Servings = NewType("Servings", int)
// Description = NewType("Description", str)

// Lang = NewType("Lang", str)


type Duration =
    | Second of decimal
    | Minutes of decimal
    | Hour of decimal
    | Day of decimal
// s = UnitFactory.unit("seconds", "s").store_as(milli)
// min = UnitFactory.unit("minutes", "min").derived(s, Converters.linear(60))
// H = UnitFactory.unit("hours", "h").derived(min, Converters.linear(60))
// days = UnitFactory.unit("days", "d").derived(H, Converters.linear(24))


type Energy =
    | Joule of decimal
    | Kilojoule of decimal
    | Calorie of decimal
    | Kilocalorie of decimal
// J = UnitFactory.unit("joules", "J")
// kJ = UnitFactory.composite(kilo, J)
// cal = UnitFactory.unit("calories", "cal").derived(J, Converters.linear("4.184"))
// kcal = UnitFactory.composite(kilo, cal)


type Mass =
    | Kilogram of decimal
    | Gram of decimal
    | Milligram of decimal
    | Microgram of decimal

type Length =
    | Meter of decimal
    | Decimeter of decimal
    | Centimeter of decimal
    | Millimeter of decimal

type Temperature =
    | Celsius of decimal
    | Fahrenheit of decimal

// C = UnitFactory.unit("celsius", "°C")
// F = UnitFactory.unit("fahrenheit", "°F").derived(
//     C, Converters.affine("5/9", Fraction("5/9") * -32)
// )

type Volume =
    | Litre of decimal
    | Decilitre of decimal
    | Centilitre of decimal
    | Millilitre of decimal
    | Cup of decimal
    | Handfull of decimal
    | TBS of decimal
    | TSP of decimal
    | Pinch of decimal

// l = UnitFactory.unit("litre", "l").store_as(milli)
// ml = UnitFactory.composite(milli, l)
// cl = UnitFactory.composite(centi, l)
// dl = UnitFactory.composite(deci, l)
// cup = UnitFactory.unit("cup", "cup").derived(ml, Converters.linear(250))
// handful = UnitFactory.unit("handful", "handful").derived(
//     ml, Converters.linear(125)
// )
// tbs = UnitFactory.unit("table spoon", "tbs").derived(ml, Converters.linear(15))
// tsp = UnitFactory.unit("tea spoon", "tsp").derived(ml, Converters.linear(5))
// pinch = UnitFactory.unit("pinch", "pinch").derived(ml, Converters.linear("1/2"))

// TODO: interval
// type Interval =


type Quantity =
    | Length of Length
    | Mass of Mass
    | Volume of Volume
    | Scalar of decimal
    | None

// Qty = Length | Mass | Scalar | Volume | None


// type Qty = Length of int<m>
//          | Mass of int<kg>
//          | Scalar of int
//          | Volume of int<L>
//          | None

// @dataclass(frozen=True)
// class Ingredient:
//     qty: Qty
//     label: str
//     aliment: Reference

// Part = str | Duration | Interval | Length | Mass | Scalar | Temperature | Volume

// @dataclass(frozen=True)
// class Step:
//     ingredients: list[Ingredient]
//     parts: list[Part]

type Reference =
    | Reference of string
    | None

type Name = Name of string
type Source = Source of string
type Lang = Lang of string
type Servings = Servings of int
type Tag = Tag of string

type Tags = Tag list

type Durations =
    { Waiting: string
      Preparing: string
      Cooking: string
      Baking: string }

type Description = Description of string

type Ingredient =
    { Quantity: Quantity
      Label: string
      Aliment: Reference }

type Ingredients = Ingredient list

type InstructionPart =
    | Text of string
    | Duration of Duration
    | Length of Length
    | Mass of Mass
    | Scalar of decimal
    | Temperature of Temperature

// Part = str | Duration | Interval | Length | Mass | Scalar | Temperature | Volume

type Instruction = InstructionPart list



type Step =
    { Instruction: Instruction
      Ingredients: Ingredients }

type Steps = Step list

type Recipe =
    { Reference: Reference
      Name: Name
      Source: Source
      Lang: Lang
      Servings: Servings
      Durations: Durations
      Tags: Tags
      Description: Description
      Steps: Steps }
