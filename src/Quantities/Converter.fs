namespace Quantities

module Converter =

    type T =
        | Identity
        | Affine of factor: Numeric.T * offset: Numeric.T
        | Composite of first: T * second: T
        | Inverse of T

    let rec convertTo converter value =
        match converter with
        | Identity -> value
        | Affine(factor, offset) -> factor * value + offset
        | Composite(first, second) -> value |> convertTo first |> convertTo second
        | Inverse inner -> convertFrom inner value

    and convertFrom converter value =
        match converter with
        | Identity -> value
        | Affine(factor, offset) -> (value - offset) / factor
        | Composite(first, second) -> value |> convertFrom second |> convertFrom first
        | Inverse inner -> convertTo inner value

    let identity = T.Identity
    let linear factor = T.Affine(factor, Numeric.zero)
    let affine factor offset = T.Affine(factor, offset)
    let compose first second = T.Composite(first, second)
    let inverse inner = T.Inverse inner
