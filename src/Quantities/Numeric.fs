namespace Quantities


module Numeric =
    type T = Fractions.Fraction

    let zero = Fractions.Fraction.Zero

    let fromString value =
        Fractions.Fraction.FromString(value, System.Globalization.CultureInfo.InvariantCulture)

    let fromInt (value: int) = Fractions.Fraction(value)
