namespace Quantities

module Quantities =
    type T<'U> = { magnitue: decimal; unit: 'U }
