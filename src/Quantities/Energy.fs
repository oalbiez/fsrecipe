namespace Quantities

module Energy =

    let J = UnitDef.Simple("joules", "J")
    let KJ = UnitDef.Composite(UnitDef.Kilo, J)

    let Cal =
        UnitDef.Derived("calories", "cal", J, Converter.linear (Numeric.fromString "4.184"))

    let KCal = UnitDef.Composite(UnitDef.Kilo, Cal)

    type Unit =
        | J
        | KJ
        | Cal
        | KCal

    let private CalorieToJoules = decimal 4.184
    let private Kilo = decimal 1000

    type Energy =
        | Joule of decimal
        | Kilojoule of decimal
        | Calorie of decimal
        | Kilocalorie of decimal

    let asJoules energy =
        match energy with
        | Joule v -> Joule v
        | Kilojoule v -> Joule(v * Kilo)
        | Calorie v -> Joule(v * CalorieToJoules)
        | Kilocalorie v -> Joule((v * CalorieToJoules * Kilo))

    let asKilojoules energy =
        match energy with
        | Joule v -> Kilojoule(v / Kilo)
        | Kilojoule v -> Kilojoule(v)
        | Calorie v -> Kilojoule(v * CalorieToJoules / Kilo)
        | Kilocalorie v -> Kilojoule((v * CalorieToJoules))

    let asCalories energy =
        match energy with
        | Joule v -> Calorie(v / CalorieToJoules)
        | Kilojoule v -> Calorie(v * Kilo / CalorieToJoules)
        | Calorie v -> Calorie(v)
        | Kilocalorie v -> Calorie(v * Kilo)

    let asKilocalories energy =
        match energy with
        | Joule v -> Kilocalorie(v / CalorieToJoules / Kilo)
        | Kilojoule v -> Kilocalorie(v / CalorieToJoules)
        | Calorie v -> Kilocalorie(v * Kilo)
        | Kilocalorie v -> Kilocalorie(v)

    let render energy =
        match energy with
        | Joule v -> $"{v}J"
        | Kilojoule v -> $"{v}KJ"
        | Calorie v -> $"{v}Cal"
        | Kilocalorie v -> $"{v}KCal"

    let magnitude energy =
        match energy with
        | Joule v -> v
        | Kilojoule v -> v
        | Calorie v -> v
        | Kilocalorie v -> v
