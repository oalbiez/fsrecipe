namespace Quantities

module UnitDef =
    type T =
        | Simple of name: string * symbol: string
        | Prefix of name: string * symbol: string * converter: Converter.T
        | Derived of name: string * symbol: string * parent: T * converter: Converter.T
        | Composite of prefix: T * parent: T

    let derived name symbol parent converter =
        T.Derived(name, symbol, parent, converter)

    let rec symbolOf unit =
        match unit with
        | Simple(_, symbol) -> symbol
        | Prefix(_, symbol, _) -> symbol
        | Derived(_, symbol, _, _) -> symbol
        | Composite(prefix, inner) -> $"{symbolOf prefix}{symbolOf inner}"

    let rec converterOf unit =
        match unit with
        | Simple(_, _) -> Converter.Identity
        | Prefix(_, _, converter) -> converter
        | Derived(_, _, parent, converter) -> Converter.Composite(converter, converterOf parent)
        | Composite(prefix, parent) -> Converter.Composite(converterOf prefix, converterOf parent)

    let rec rootOf unit =
        match unit with
        | Simple(_, _) -> unit
        | Prefix(_, _, _) -> unit
        | Derived(_, _, parent, _) -> rootOf parent
        | Composite(_, parent) -> rootOf parent

    let Micro = Prefix("micro", "µ", Converter.linear (Numeric.fromString "0.000001"))
    let Milli = Prefix("milli", "m", Converter.linear (Numeric.fromString "0.001"))
    let Centi = Prefix("centi", "c", Converter.linear (Numeric.fromString "0.01"))
    let Deci = Prefix("deci", "d", Converter.linear (Numeric.fromString "0.1"))
    let Kilo = Prefix("kilo", "k", Converter.linear (Numeric.fromString "1000"))
