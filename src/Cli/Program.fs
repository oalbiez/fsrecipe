﻿// For more information see https://aka.ms/fsharp-console-apps
printfn "Hello from F#"

open Quantities


let e = Energy.Kilojoule(decimal 10)

printfn "%s" (Energy.render (Energy.asJoules e))
printfn "%M" (decimal "0.1")
